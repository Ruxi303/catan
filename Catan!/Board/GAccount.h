#pragma once
#include"StartGameh.h"
namespace Board {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for GAccount
	/// </summary>
	public ref class GAccount : public System::Windows::Forms::Form
	{
	public:
		GAccount(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~GAccount()
		{
			if (components)
			{
				delete components;
			}
		}


	private: System::Windows::Forms::TextBox^  NameText;
	private: System::Windows::Forms::TextBox^  LastNameText;
	private: System::Windows::Forms::TextBox^  NicknameText;
	private: System::Windows::Forms::TextBox^  UsernameText;
	private: System::Windows::Forms::TextBox^  PasswordText;
	private: System::Windows::Forms::TextBox^  AgeText;
	private: System::Windows::Forms::Label^  InsertFirstName;
	private: System::Windows::Forms::Label^  InsertLastName;

	private: System::Windows::Forms::Label^  InsertNickname;

	private: System::Windows::Forms::Label^  InsertUsername;

	private: System::Windows::Forms::Label^  InsertPassword;

	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  UsernameLog;
	private: System::Windows::Forms::Label^  PasswordLog;
	private: System::Windows::Forms::TextBox^  UserLogin;
	private: System::Windows::Forms::TextBox^  Passlogin;




	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private: System::Windows::Forms::Label^  login;
	private: System::Windows::Forms::Label^  CreateAccount;






	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(GAccount::typeid));
			this->NameText = (gcnew System::Windows::Forms::TextBox());
			this->LastNameText = (gcnew System::Windows::Forms::TextBox());
			this->NicknameText = (gcnew System::Windows::Forms::TextBox());
			this->UsernameText = (gcnew System::Windows::Forms::TextBox());
			this->PasswordText = (gcnew System::Windows::Forms::TextBox());
			this->AgeText = (gcnew System::Windows::Forms::TextBox());
			this->InsertFirstName = (gcnew System::Windows::Forms::Label());
			this->InsertLastName = (gcnew System::Windows::Forms::Label());
			this->InsertNickname = (gcnew System::Windows::Forms::Label());
			this->InsertUsername = (gcnew System::Windows::Forms::Label());
			this->InsertPassword = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->UsernameLog = (gcnew System::Windows::Forms::Label());
			this->PasswordLog = (gcnew System::Windows::Forms::Label());
			this->UserLogin = (gcnew System::Windows::Forms::TextBox());
			this->Passlogin = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->login = (gcnew System::Windows::Forms::Label());
			this->CreateAccount = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// NameText
			// 
			this->NameText->AccessibleRole = System::Windows::Forms::AccessibleRole::Border;
			this->NameText->BackColor = System::Drawing::SystemColors::Window;
			this->NameText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->NameText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->NameText->Location = System::Drawing::Point(846, 69);
			this->NameText->Name = L"NameText";
			this->NameText->Size = System::Drawing::Size(281, 29);
			this->NameText->TabIndex = 2;
			this->NameText->TextChanged += gcnew System::EventHandler(this, &GAccount::textBox1_TextChanged);
			// 
			// LastNameText
			// 
			this->LastNameText->BackColor = System::Drawing::SystemColors::Window;
			this->LastNameText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->LastNameText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->LastNameText->Location = System::Drawing::Point(846, 110);
			this->LastNameText->Name = L"LastNameText";
			this->LastNameText->Size = System::Drawing::Size(281, 29);
			this->LastNameText->TabIndex = 3;
			this->LastNameText->TextChanged += gcnew System::EventHandler(this, &GAccount::textBox1_TextChanged_1);
			// 
			// NicknameText
			// 
			this->NicknameText->BackColor = System::Drawing::SystemColors::Window;
			this->NicknameText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->NicknameText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->NicknameText->Location = System::Drawing::Point(846, 155);
			this->NicknameText->Name = L"NicknameText";
			this->NicknameText->Size = System::Drawing::Size(281, 29);
			this->NicknameText->TabIndex = 4;
			this->NicknameText->TextChanged += gcnew System::EventHandler(this, &GAccount::textBox2_TextChanged);
			// 
			// UsernameText
			// 
			this->UsernameText->BackColor = System::Drawing::SystemColors::Window;
			this->UsernameText->Cursor = System::Windows::Forms::Cursors::AppStarting;
			this->UsernameText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->UsernameText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->UsernameText->Location = System::Drawing::Point(846, 202);
			this->UsernameText->Name = L"UsernameText";
			this->UsernameText->Size = System::Drawing::Size(281, 29);
			this->UsernameText->TabIndex = 5;
			// 
			// PasswordText
			// 
			this->PasswordText->BackColor = System::Drawing::SystemColors::Window;
			this->PasswordText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->PasswordText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->PasswordText->Location = System::Drawing::Point(846, 252);
			this->PasswordText->Name = L"PasswordText";
			this->PasswordText->PasswordChar = '*';
			this->PasswordText->Size = System::Drawing::Size(281, 29);
			this->PasswordText->TabIndex = 6;
			this->PasswordText->TextChanged += gcnew System::EventHandler(this, &GAccount::textBox4_TextChanged);
			// 
			// AgeText
			// 
			this->AgeText->BackColor = System::Drawing::SystemColors::Window;
			this->AgeText->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->AgeText->ForeColor = System::Drawing::SystemColors::ControlDark;
			this->AgeText->Location = System::Drawing::Point(846, 293);
			this->AgeText->Name = L"AgeText";
			this->AgeText->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->AgeText->Size = System::Drawing::Size(100, 29);
			this->AgeText->TabIndex = 7;
			// 
			// InsertFirstName
			// 
			this->InsertFirstName->AutoSize = true;
			this->InsertFirstName->BackColor = System::Drawing::Color::Transparent;
			this->InsertFirstName->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->InsertFirstName->ForeColor = System::Drawing::SystemColors::WindowText;
			this->InsertFirstName->Location = System::Drawing::Point(665, 72);
			this->InsertFirstName->Name = L"InsertFirstName";
			this->InsertFirstName->Size = System::Drawing::Size(140, 22);
			this->InsertFirstName->TabIndex = 8;
			this->InsertFirstName->Text = L"First Name";
			this->InsertFirstName->Click += gcnew System::EventHandler(this, &GAccount::label1_Click);
			// 
			// InsertLastName
			// 
			this->InsertLastName->AutoSize = true;
			this->InsertLastName->BackColor = System::Drawing::Color::Transparent;
			this->InsertLastName->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->InsertLastName->ForeColor = System::Drawing::SystemColors::WindowText;
			this->InsertLastName->Location = System::Drawing::Point(665, 110);
			this->InsertLastName->Name = L"InsertLastName";
			this->InsertLastName->Size = System::Drawing::Size(133, 22);
			this->InsertLastName->TabIndex = 9;
			this->InsertLastName->Text = L"Last Name";
			this->InsertLastName->Click += gcnew System::EventHandler(this, &GAccount::label1_Click_1);
			// 
			// InsertNickname
			// 
			this->InsertNickname->AutoSize = true;
			this->InsertNickname->BackColor = System::Drawing::Color::Transparent;
			this->InsertNickname->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->InsertNickname->ForeColor = System::Drawing::SystemColors::WindowText;
			this->InsertNickname->Location = System::Drawing::Point(665, 155);
			this->InsertNickname->Name = L"InsertNickname";
			this->InsertNickname->Size = System::Drawing::Size(121, 22);
			this->InsertNickname->TabIndex = 10;
			this->InsertNickname->Text = L"Nickname";
			this->InsertNickname->Click += gcnew System::EventHandler(this, &GAccount::label2_Click);
			// 
			// InsertUsername
			// 
			this->InsertUsername->AutoSize = true;
			this->InsertUsername->BackColor = System::Drawing::Color::Transparent;
			this->InsertUsername->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->InsertUsername->ForeColor = System::Drawing::SystemColors::WindowText;
			this->InsertUsername->Location = System::Drawing::Point(665, 202);
			this->InsertUsername->Name = L"InsertUsername";
			this->InsertUsername->Size = System::Drawing::Size(124, 22);
			this->InsertUsername->TabIndex = 11;
			this->InsertUsername->Text = L"Username";
			this->InsertUsername->Click += gcnew System::EventHandler(this, &GAccount::InsertUsername_Click);
			// 
			// InsertPassword
			// 
			this->InsertPassword->AutoSize = true;
			this->InsertPassword->BackColor = System::Drawing::Color::Transparent;
			this->InsertPassword->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->InsertPassword->ForeColor = System::Drawing::SystemColors::WindowText;
			this->InsertPassword->Location = System::Drawing::Point(668, 252);
			this->InsertPassword->Name = L"InsertPassword";
			this->InsertPassword->Size = System::Drawing::Size(121, 22);
			this->InsertPassword->TabIndex = 12;
			this->InsertPassword->Text = L"Password";
			this->InsertPassword->Click += gcnew System::EventHandler(this, &GAccount::label4_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::Transparent;
			this->label5->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label5->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label5->Location = System::Drawing::Point(664, 293);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(52, 22);
			this->label5->TabIndex = 13;
			this->label5->Text = L"Age";
			this->label5->Click += gcnew System::EventHandler(this, &GAccount::label5_Click);
			// 
			// UsernameLog
			// 
			this->UsernameLog->AutoSize = true;
			this->UsernameLog->BackColor = System::Drawing::Color::Transparent;
			this->UsernameLog->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->UsernameLog->Location = System::Drawing::Point(45, 72);
			this->UsernameLog->Name = L"UsernameLog";
			this->UsernameLog->Size = System::Drawing::Size(124, 22);
			this->UsernameLog->TabIndex = 14;
			this->UsernameLog->Text = L"Username";
			this->UsernameLog->Click += gcnew System::EventHandler(this, &GAccount::label1_Click_2);
			// 
			// PasswordLog
			// 
			this->PasswordLog->AutoSize = true;
			this->PasswordLog->BackColor = System::Drawing::Color::Transparent;
			this->PasswordLog->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->PasswordLog->Location = System::Drawing::Point(48, 137);
			this->PasswordLog->Name = L"PasswordLog";
			this->PasswordLog->Size = System::Drawing::Size(121, 22);
			this->PasswordLog->TabIndex = 15;
			this->PasswordLog->Text = L"Password";
			// 
			// UserLogin
			// 
			this->UserLogin->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->UserLogin->Location = System::Drawing::Point(228, 72);
			this->UserLogin->Name = L"UserLogin";
			this->UserLogin->Size = System::Drawing::Size(171, 29);
			this->UserLogin->TabIndex = 16;
			// 
			// Passlogin
			// 
			this->Passlogin->Font = (gcnew System::Drawing::Font(L"Ravie", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->Passlogin->Location = System::Drawing::Point(228, 137);
			this->Passlogin->Name = L"Passlogin";
			this->Passlogin->Size = System::Drawing::Size(191, 29);
			this->Passlogin->TabIndex = 17;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(128, 200);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(122, 31);
			this->button1->TabIndex = 18;
			this->button1->Text = L"LogIn";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &GAccount::button1_Click);
			this->button1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &GAccount::LogIn_MouseClick);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(846, 347);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 19;
			this->button2->Text = L"Create";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// login
			// 
			this->login->AutoSize = true;
			this->login->BackColor = System::Drawing::Color::Transparent;
			this->login->Font = (gcnew System::Drawing::Font(L"Ravie", 18, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->login->ForeColor = System::Drawing::SystemColors::WindowText;
			this->login->Location = System::Drawing::Point(155, 21);
			this->login->Name = L"login";
			this->login->Size = System::Drawing::Size(116, 34);
			this->login->TabIndex = 19;
			this->login->Text = L"Log In";
			// 
			// CreateAccount
			// 
			this->CreateAccount->AutoSize = true;
			this->CreateAccount->BackColor = System::Drawing::Color::Transparent;
			this->CreateAccount->Font = (gcnew System::Drawing::Font(L"Ravie", 18, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->CreateAccount->ForeColor = System::Drawing::SystemColors::WindowText;
			this->CreateAccount->Location = System::Drawing::Point(772, 21);
			this->CreateAccount->Name = L"CreateAccount";
			this->CreateAccount->Size = System::Drawing::Size(277, 34);
			this->CreateAccount->TabIndex = 21;
			this->CreateAccount->Text = L"Create Account";
			// 
			// GAccount
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->ClientSize = System::Drawing::Size(1278, 411);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->login);
			this->Controls->Add(this->CreateAccount);
			this->Controls->Add(this->PasswordLog);
			this->Controls->Add(this->UsernameLog);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->Passlogin);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->InsertPassword);
			this->Controls->Add(this->UserLogin);
			this->Controls->Add(this->InsertUsername);
			this->Controls->Add(this->InsertNickname);
			this->Controls->Add(this->InsertLastName);
			this->Controls->Add(this->InsertFirstName);
			this->Controls->Add(this->AgeText);
			this->Controls->Add(this->PasswordText);
			this->Controls->Add(this->UsernameText);
			this->Controls->Add(this->NicknameText);
			this->Controls->Add(this->LastNameText);
			this->Controls->Add(this->NameText);
			this->Cursor = System::Windows::Forms::Cursors::No;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Name = L"GAccount";
			this->Text = L"GAccount";
			this->TransparencyKey = System::Drawing::SystemColors::ActiveBorder;
			this->Load += gcnew System::EventHandler(this, &GAccount::GAccount_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void GAccount_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void textBox1_TextChanged_1(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label1_Click_1(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label4_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void InsertUsername_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label1_Click_2(System::Object^  sender, System::EventArgs^  e) {
}

private: System::Void LogIn_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	StartGameh start;
	start.ShowDialog();
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
}
};
}
