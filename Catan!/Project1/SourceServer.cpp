#include<iostream>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

int main()
{
	// *** Initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
	if (iResult != 0)
	{
		std::cout << "WSAStartup failed:" << iResult;
		return 1;
	}

	// *** Create socket ***
#define DEFAULT_PORT "27015"
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_STREAM;	// stream
	hints.ai_protocol = IPPROTO_TCP;	// tcp
	hints.ai_flags = AI_PASSIVE;		// we intend to use the socket in a call to bind

	// Resolve the local address and port to be used by the server
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		std::cout << "getaddrinfo failed:" << iResult;
		WSACleanup();
		return 1;
	}

	SOCKET ListenSocket = INVALID_SOCKET;
	// Create a SOCKET for the server to listen for client connections
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET)
	{
		std::cout << "Error at socket():" << WSAGetLastError();
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// *** Binding the socket ***
	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		std::cout << "bind failed with error: " << WSAGetLastError();
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// 'result' not needed anymore so free it
	freeaddrinfo(result);


	// *** Listening on the socket ***
	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Listen failed with error:" << WSAGetLastError();
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// *** Accept a connection ***
	// There are multiple techniques to accept client connections,
	// and multiple techniques to accept multiple client connections.
	// In this example however a single connection is accepted.
	SOCKET ClientSocket = INVALID_SOCKET;
	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET)
	{
		std::cout << "accept failed:" << WSAGetLastError();
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// No longer need server socket
	closesocket(ListenSocket);

	// *** Recieve and send data ***
#define DEFAULT_BUFLEN 512
	char recvbuf[DEFAULT_BUFLEN];
	char sendbuf[DEFAULT_BUFLEN];
	int iRecvResult, iSendResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Receive until the peer shuts down the connection
	do
	{

		iRecvResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iRecvResult > 0)
		{
			std::cout << "Bytes received:" << iRecvResult;
			std::cout << "Buffer content:" << iRecvResult << " " << recvbuf;

			// Thank the client :)
			std::cout << sendbuf << "Thank you, I received " << iRecvResult << "bytes from you!";
			iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
			if (iSendResult == SOCKET_ERROR)
			{
				std::cout << "send failed: " << WSAGetLastError() << "\n";
				closesocket(ClientSocket);
				WSACleanup();
				return 1;
			}
			std::cout << "Bytes sent:" << iSendResult << "\n";
		}
		else if (iRecvResult == 0)
			std::cout << "Connection closing...\n";
		else
		{
			std::cout << "recv failed: " << WSAGetLastError() << "\n";
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

	} while (iRecvResult > 0);

	// *** Disconnecting the Server ***
	// shutdown the send half of the connection since no more data will be sent
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown failed: " << WSAGetLastError() << "\n";
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}
