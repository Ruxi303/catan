#pragma once
#include<iostream>
#include"ResourcesCards.h"
#include"RaiffaisenBank.h"
#include<memory>
class Road: public ResourcesCards, public RaiffaisenBank
{
protected:	
//int totalRoadOnBoard;
std::shared_ptr<int> totalRoadOnBoard = std::make_shared<int>();
static const auto maxRoad = 15;
public:
	Road();
	bool VerifIfExist();
	// Check if you have the necessary cards
	bool VerifCost();
	void CreateRoad();
	virtual ~Road();
};

