#include "City.h"



City::City()
{
	*totalCitiesOnBoard = 0;
}

bool City::VerifIfExist()
{
	if (*totalCitiesOnBoard < maxCity)
		return true;
	return false;
}

bool City::VerifCost()
{
	if (resources[3].second > 1)
		if (resources[4].second > 2)
			return true;
	return false;
}
bool City::CreateCity()
{
	try
	{
		if (VerifCost() == true && VerifIfExist() == true)
		{
			resources[3].second -= 1;
			resources[4].second -= 2;
			resourceCardsInBank[3].second++;
			resourceCardsInBank[4].second += 2;
			*totalCitiesOnBoard+=1;
			return true;
			//VerifPos();
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
		
	}
	return false;
}
City::~City()
{
}
