#pragma once
#include"City.h"
#include"Country.h"
#include"DevelopmentCards.h"
#include"SpecialCards.h"

class Victory :public SpecialCards, public City, public Country
{
public:
	static const auto winPoint = 12;
	int point;
	Victory();
	void PointLongRoad();
	void PointLargArmy();
	void PointCity();
	void PointCountry();
	void VictoryPoint();
	/*int TotalPoint(std::string choise);*/
	bool EndGame();
	~Victory();
};

