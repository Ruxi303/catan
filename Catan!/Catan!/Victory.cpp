#include "Victory.h"



Victory::Victory()
{
	point = 0;
}

void Victory::PointLongRoad()
{
	if (SpecialCards::LongRoadVerification() == true)
	{
		point += 2;
	}

}

void Victory::PointLargArmy()
{
	if (SpecialCards::LargeArmyVerification() == true)
	{
		point += 2;
	}
}

void Victory::PointCity()
{
	if (CreateCity()==true)
	{
		point += 2;
	}
}

void Victory::PointCountry()
{
	if (CreateCountry() == true)
	{
		point ++;
	}
}

void Victory::VictoryPoint()
{
	if (VictoryPointCard() == true)
	{
		point++;
	}

}


//void Victory::TotalPoint(std::string choise)
//{
//	if (choise == "Road")
//		point =PointLongRoad();
//	if (choise == "City")
//		point = PointCity();
//	if (choise == "Country")
//		point = PointCountry();
//}
bool Victory::EndGame()
{
	if (point > winPoint)
		return true;
	return false;
}
Victory::~Victory()
{
}
