#include "Player.h"



Player::Player()
{
	Road::Road();
	City::City();
	Country::Country();
	ResourcesCards::ResourcesCards();
}


void Player::LogInNewAccount(std::string const& choise)
{
	if (choise == "LogIn")
		Account::UserLogIn();
	if (choise == "NewAccount")
		Account::CreateAccount();
}



void Player::ChooseAction(std::string const & choise)
{
	DevelopmentOnTheBoard::Choise(choise);
}


Player::~Player()
{
}
