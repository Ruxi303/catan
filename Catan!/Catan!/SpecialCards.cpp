#include "SpecialCards.h"



SpecialCards::SpecialCards()
{
	playerLongRoadNumber = 0;
	playerLargArmyNumber = 0;
}


bool SpecialCards::LongRoadVerification()
{
	if (*totalRoadOnBoard > playerLongRoadNumber)
	{
		playerLongRoadNumber = *totalRoadOnBoard;
		return true;
	}
	return false;
}

bool SpecialCards::LargeArmyVerification()
{
	if (std::get<2>(develop[0]) > playerLargArmyNumber)
	{
		playerLargArmyNumber = std::get<2>(develop[0]);
		Knight();
		return true;
	}
	return false;
}


SpecialCards::~SpecialCards()
{
}
