#pragma once
#include<iostream>
#include"ResourcesCards.h"
#include"RaiffaisenBank.h"
#include<memory>
class City:public ResourcesCards, public RaiffaisenBank
{
protected:
	static const auto maxCity = 5;
	//int totalCitiesOnBoard;
	std::shared_ptr<int> totalCitiesOnBoard = std::make_shared<int>();
public:
	City();
	bool VerifIfExist();
	bool VerifCost();
	//bool VerifPos();
	bool CreateCity();
	~City();
};

