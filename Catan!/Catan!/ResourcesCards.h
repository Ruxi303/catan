#pragma once
#include<iostream>
//#include<vector>
#include<Windows.h>
#include<array>
#include<string>
#include"RaiffaisenBank.h"
class ResourcesCards
{ 
public:
	//We have 5 type of resources card 
	static const size_t resourcesCardNumber=5;
	//Memorize the name of the resources and how many cards we have of every type
	std::array < std::pair<std::string, int>,resourcesCardNumber > resources;
	enum Resources : uint8_t
	{
		None,
		Stone,
		Sheep,
		Brick,
		Grain,
		Wood
	};
	ResourcesCards();
	//if we have 5 resources of the same type we can change with one of other resources
	void Exchange();
	//void ChangeBetweenPlayers(int positionResource);
	void PickResource(std::istream &in,std::string takeCard, ResourcesCards& other);
	void ThiefCards();
	virtual ~ResourcesCards();
};

