#include "Country.h"



Country::Country()
{
	*totalCountryOnBoard = 1;
}
bool Country::VerifIfExist()
{
	if (*totalCountryOnBoard < maxCountry)
		return true;
	return false;
}

bool Country::VerifCost()
{
	if (ResourcesCards::resources[0].second > 0)
		if (ResourcesCards::resources[4].second > 0)
			if (ResourcesCards::resources[3].second > 0)
				if (ResourcesCards::resources[1].second > 0)
					return true;
	return false;
}


bool Country::CreateCountry()
{
	try
	{
		if (VerifCost() == true && VerifIfExist() == true)
		{
			resources[0].second--;
			resources[4].second--;
			resourceCardsInBank[0].second++;
			resourceCardsInBank[4].second++;
			*totalCountryOnBoard+=1;
			//VerifPos();
			return true;
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
		
	}
	return false;
}

Country::~Country()
{
}
