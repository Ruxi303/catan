#pragma once
#include<tuple>
#include<iostream>
#include"ResourcesCards.h"
class Dice:public ResourcesCards
{
public:
	Dice();
	std::shared_ptr<int> dice1 = std::make_shared<int>();
	std::shared_ptr<int> dice2 = std::make_shared<int>();
	/*int dice1;
	int dice2;*/
	int thiefPos;
	void Thief();
	template <class Type>
	Type GetMax(Type *dice1, Type *dice2);

	~Dice();
};

template<class Type>
inline Type Dice::GetMax(Type * dice1, Type * dice2)
{
	return (*dice1>*dice2?*dice1:*dice2);
}
