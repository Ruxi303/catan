#pragma once
#include<iostream>
#include"ResourcesCards.h"
#include"RaiffaisenBank.h"
#include"Road.h"
#include<tuple>
#include<vector>
#include<string>
#include<array>

class DevelopmentCards: public Road 
{
public:
	static const size_t numberDev=5;
	std::array<std::tuple<std::string, int, int>, numberDev> develop; // first is for name of the card, second is for how many cards you have and third is for how many cards you use 
	int card;
	enum class Development : uint8_t
	{
		knightCard = 0,// lets the player move the robber
		roadBuilding = 0,// player can place 2 roads as if they just built them
		yearOfPlenty = 0,// the player can draw 2 resource cards of their choice from the bank
		Monopoly = 0,// player can claim all resource cards of a specific declared type
		victoryPointCard = 0// 1 additional Victory Point is added to the owners total and doesn't need to be played to win.
	};
	// Check if you have the necessary cards
	DevelopmentCards();
	bool VerifCost();
	void Knight();
	auto Search(const std::string& val);
	void roadBuild();
	void YearofPlenty(std::string choise);
	void Monopoly(std::string choise);
	bool VictoryPointCard();
	void DevTot();
	~DevelopmentCards();
};

