﻿#pragma once
#include<iostream>
#include<string.h>
#include"Road.h"
#include<tuple>
#include"Road.h"
#include"DevelopmentCards.h"

class SpecialCards:public DevelopmentCards
{
	public:
	int playerLongRoadNumber;
	int playerLargArmyNumber;
public:
	SpecialCards();
	enum class SpecCards : uint8_t
	{
		longestRoad,
		largestArmy
	};
	//check if the actual Player fulfills the condition for the longest road 
	bool LongRoadVerification();
	bool LargeArmyVerification();
	~SpecialCards();
};

