
#pragma once
#include<iostream>
#include"ResourcesCards.h"
#include"RaiffaisenBank.h"
#include<memory>
class Country: public ResourcesCards, public RaiffaisenBank
{
public:
	Country();
	static const auto maxCountry = 5;
	//int totalCountryOnBoard;
	std::shared_ptr<int> totalCountryOnBoard = std::make_shared<int>();
	bool VerifIfExist();
	bool VerifCost();
	//bool VerifPos();
	bool CreateCountry();
	~Country();
};

