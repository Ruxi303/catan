#include "Road.h"


Road::Road()
{
	*totalRoadOnBoard = 2;
}

bool Road::VerifIfExist()
{
	if (*totalRoadOnBoard < maxRoad)
		return true;
	return false;
}

bool Road::VerifCost()
{
	if (resources[0].second > 0)
		if (resources[4].second > 0)
			return true;
	return false;
}

Road::~Road()
{
}

void Road::CreateRoad()
{
	try
	{
		if (VerifCost() == true && VerifIfExist() == true)
		{
			resources[0].second--;
			resources[4].second--;
			resourceCardsInBank[0].second++;
			resourceCardsInBank[4].second++;
			*totalRoadOnBoard+=1;
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}
